﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div  id="app">
            <select @change="CountyChange" v-model = "CountySelected">
              <option v-for="v in AllMember" >{{v}}</option>            
            </select>

            <select v-model="CitySelected">
              <option v-for="v in OneGroupCity" >{{v}}</option>            
            </select>
            <br /><br />
            <div>您已經挑選了：{{CountySelected}} - {{CitySelected}}</div>
        </div>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    
    <script src="./js/Default.js"></script>
</body>
</html>
