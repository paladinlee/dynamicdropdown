﻿var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        address: [
            { 'County': '台北市', 'City': '中正區' },
            { 'County': '台北市', 'City': '大同區' }, 
            { 'County': '台北市', 'City': '中山區' },
            { 'County': '台北市', 'City': '松山區' },
            { 'County': '台北市', 'City': '大安區' },
            { 'County': '台北市', 'City': '萬華區' },
            { 'County': '台北市', 'City': '信義區' },
            { 'County': '台北市', 'City': '士林區' },
            { 'County': '台北市', 'City': '北投區' },
            { 'County': '台北市', 'City': '內湖區' },
            { 'County': '台北市', 'City': '南港區' },
            { 'County': '台北市', 'City': '文山區' },
            { 'County': '新竹縣', 'City': '竹北市' },
            { 'County': '新竹縣', 'City': '湖口鄉' },
            { 'County': '新竹縣', 'City': '新豐鄉' },
            { 'County': '新竹縣', 'City': '新埔鎮' },
            { 'County': '新竹縣', 'City': '關西鎮' },
            { 'County': '新竹縣', 'City': '竹東鎮' }
        ],
        CountySelected: '',
        CitySelected: '',
    },    
    methods: {
        GetAllCounty: function () {
            var vAll = [];
            this.address.forEach(element => { if (vAll.indexOf(element.County) < 0) { vAll.push(element.County); } });
            console.log('vAll:', vAll);
            return vAll;
        },
        CountyChange: function () {
            //縣市有異動
            console.log('County Change');

            //清空鄉鎮內容
            this.CitySelected = '';
        },
        GetCity: function (vCounty) {
            var vAll = [];
            this.address.forEach(element => { if (vAll.indexOf(element.City) < 0 && element.County == vCounty) { vAll.push(element.City); } });
            console.log('vAll:', vAll);
            return vAll;
        }
    },
    computed: {
        AllMember: function () {
            return this.GetAllCounty();
        },
        OneGroupCity: function () {
            return this.GetCity(this.CountySelected);
        }
    }
})